Scripts I made for use with KWin, the KDE window manager

## Installation
Copy any of the scripts to *$HOME/.local/share/kwin/scripts*, creating any directories that don't already exist

## Licensing
License of all add-ons found here: GNU GPLv2 or later
